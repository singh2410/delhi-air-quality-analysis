#!/usr/bin/env python
# coding: utf-8

# # An analysis of the COVID-19's Lockdown effect on the Pollution level in Delhi
# #By- Aarush Kumar
# #Dated: November 11,2021

# In[1]:


from IPython.display import Image
Image(url='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.explicit.bing.net%2Fth%3Fid%3DOIF.%252blcoTFm2dgs2pc71f7wIlQ%26pid%3DApi&f=1')


# In[2]:


import numpy as np 
import pandas as pd 
import os
# Visualisation libraries
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns
import plotly.express as px
import plotly.graph_objects as go


# In[3]:


url='/home/aarush100616/Downloads/Projects/Delhi Pollution/Data/city_day.csv'
city_day_data=pd.read_csv(url)
# Extract delhi's data 
delhi_data=city_day_data.groupby('City').get_group('Delhi')


# In[4]:


delhi_data.head()


# ## Handling Missing values

# In[5]:


def missing_values_table(df):
        # Total missing values
        mis_val = df.isnull().sum()
        
        # Percentage of missing values
        mis_val_percent = 100 * df.isnull().sum() / len(df)
        
        # Make a table with the results
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        
        # Rename the columns
        mis_val_table_ren_columns = mis_val_table.rename(
        columns = {0 : 'Missing Values', 1 : '% of Total Values'})
        
        # Sort the table by percentage of missing descending
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:,1] != 0].sort_values(
        '% of Total Values', ascending=False).round(1)
        
        # Print some summary information
        print ("Your selected dataframe has " + str(df.shape[1]) + " columns.\n"      
            "There are " + str(mis_val_table_ren_columns.shape[0]) +
              " columns that have missing values.")
        
        # Return the dataframe with missing information
        return mis_val_table_ren_columns


# In[6]:


missing_values_table(delhi_data)


# In[7]:


delhi_data.interpolate(limit_direction="both",inplace=True)


# In[8]:


missing_values_table(delhi_data)


# In[9]:


delhi_data['AQI_Bucket'].iloc[0]


# In[10]:


for i,each in enumerate(delhi_data['AQI_Bucket']):
    if pd.isnull(delhi_data['AQI_Bucket'].iloc[i]):
        if delhi_data['AQI'].iloc[i]>=0.0 and delhi_data['AQI'].iloc[i]<=50.0:
            delhi_data['AQI_Bucket'].iloc[i]='Good'
        elif delhi_data['AQI'].iloc[i]>=51.0 and delhi_data['AQI'].iloc[i]<=100.0:
            delhi_data['AQI_Bucket'].iloc[i]='Satisfactory'
        elif delhi_data['AQI'].iloc[i]>=101.0 and delhi_data['AQI'].iloc[i]<=200.0:
            delhi_data['AQI_Bucket'].iloc[i]='Moderate'
        elif delhi_data['AQI'].iloc[i]>=201.0 and delhi_data['AQI'].iloc[i]<=300.0:
            delhi_data['AQI_Bucket'][i]='Poor'
        elif delhi_data['AQI'].iloc[i]>=301.0 and delhi_data['AQI'].iloc[i]<=400.0:
            delhi_data['AQI_Bucket'].iloc[i]='Very Poor'
        else:
            delhi_data['AQI_Bucket'].iloc[i]='Severe'


# In[11]:


delhi_data.head(2)


# In[12]:


fig = px.line(delhi_data, x="Date", y="PM2.5")
fig.show()


# In[13]:


df_year_19_20=delhi_data[delhi_data['Date']>='2019']


# In[14]:


fig = px.line(df_year_19_20, x="Date", y="PM2.5")
fig.show()


# In[15]:


Mar_may_2019=delhi_data[(delhi_data['Date'] >= '2019-03') & (delhi_data['Date'] <= '2019-05')]
Mar_may_2020=delhi_data[(delhi_data['Date'] >= '2020-03') & (delhi_data['Date'] <= '2020-05')]


# In[16]:


plt.style.use('fivethirtyeight')


# In[17]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=Mar_may_2019['Date'], y=Mar_may_2019['PM2.5'],
                    mode='lines+markers',
                    name='PM2.5 levels of 2019'))
fig.add_trace(go.Scatter(x=Mar_may_2019['Date'], y=Mar_may_2020['PM2.5'],
                    mode='lines+markers',
                    name='PM2.5 levels of 2020'))

fig.show()


# In[18]:


fig = px.line(delhi_data, x="Date", y="PM10")
fig.show()


# In[19]:


fig = px.line(df_year_19_20, x="Date", y="PM10")
fig.show()


# In[20]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=Mar_may_2019['Date'], y=Mar_may_2019['PM10'],
                    mode='lines+markers',
                    name='PM10 levels of delhi march and april month of 2019'))
fig.add_trace(go.Scatter(x=Mar_may_2019['Date'], y=Mar_may_2020['PM10'],
                    mode='lines+markers',
                    name='PM10 levels of delhi march and april month of 2020'))
fig.show()


# In[21]:


fig = px.line(delhi_data, x="Date", y="NO")
fig.show()


# In[22]:


fig = px.line(df_year_19_20, x="Date", y="NO")
fig.show()


# In[23]:


march24_2019=delhi_data[(delhi_data['Date'] >= '2019-03-23') & (delhi_data['Date'] <= '2019-04-15')]
march24_2020=delhi_data[(delhi_data['Date'] >= '2020-03-23') & (delhi_data['Date'] <= '2020-04-15')]


# In[24]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2019['NO'],
                    mode='lines+markers',
                    name='NO levels of delhi from 23-march-2019  to 15-april 2019'))
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2020['NO'],
                    mode='lines+markers',
                    name='NO levels of delhi from 23-march-2020  to 15-april 2020'))
fig.show()


# In[25]:


fig = px.line(delhi_data, x="Date", y="NO2")
fig.show()


# In[26]:


fig = px.line(df_year_19_20, x="Date", y="NO2")
fig.show()


# In[27]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2019['NO2'],
                    mode='lines+markers',
                    name='NO2 levels of delhi from 23-march-2019  to 15-april 2019'))
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2020['NO2'],
                    mode='lines+markers',
                    name='NO2 levels of delhi from 23-march-2020  to 15-april 2020'))
fig.show()


# In[28]:


fig = px.line(delhi_data, x="Date", y="O3")
fig.show()


# In[29]:


fig = px.line(df_year_19_20, x="Date", y="O3")
fig.show()


# In[30]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2019['O3'],
                    mode='lines+markers',
                    name='O3 levels of delhi from 23-march-2019  to 15-april 2019'))
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2020['O3'],
                    mode='lines+markers',
                    name='O3 levels of delhi from 23-march-2020  to 15-april 2020'))
fig.show()


# In[31]:


fig = px.line(delhi_data, x="Date", y="SO2")
fig.show()


# In[32]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2019['SO2'],
                    mode='lines+markers',
                    name='SO2 levels of delhi from 23-march-2019  to 15-april 2019'))
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2020['SO2'],
                    mode='lines+markers',
                    name='SO2 levels of delhi from 23-march-2020  to 15-april 2020'))
fig.show()


# In[33]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=delhi_data['Date'], y=delhi_data['Benzene'],
                    mode='lines',
                    name='Benzene level'))
fig.add_trace(go.Scatter(x=delhi_data['Date'], y=delhi_data['Toluene'],
                    mode='lines',
                    name='Toluene level'))
fig.add_trace(go.Scatter(x=delhi_data['Date'], y=delhi_data['Xylene'],
                    mode='lines',
                    name='Xylene level'))
fig.show()


# In[34]:


data_2019=delhi_data[(delhi_data['Date'] >= '2019') & (delhi_data['Date'] < '2020')]
data_2019['BTX']=data_2019['Benzene'] + data_2019['Toluene'] + data_2019['Xylene']


# In[35]:


fig = px.line(data_2019, x="Date", y="BTX")
fig.show()


# In[36]:


fig = px.line(delhi_data, x="Date", y="AQI")
fig.show()


# In[37]:


fig = go.Figure()
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2019['AQI'],
                    mode='lines+markers',
                    name='AQI levels of delhi from 23-march-2019  to 15-april 2019'))
fig.add_trace(go.Scatter(x=march24_2019['Date'], y=march24_2020['AQI'],
                    mode='lines+markers',
                    name='AQI levels of delhi from 23-march-2020  to 15-april 2020'))
fig.show()


# In[38]:


march24_2019['AQI'].mean()


# In[39]:


march24_2020['AQI'].mean()


# As AQI Levels changes POOR to SATISFACTORY due to lockdown as compared to last year
# Much of the world is seeing significant reductions in many air pollutant and greenhouse gas emissions due to efforts to stem the covid-19 pandemic. This is a stark confirmation of the contribution of our everyday activities to sources of emissions of the air pollutants that we breathe and the greenhouse gases that drive global warming. The speed with which emissions have fallen shows how quickly we can improve our environment when motivated and how vulnerable we are living in degraded environments.
